**Android Practical**<br>
_Time: Upto 4 Hours_<br><br>
Develop Android application for below listed features with respect to guidelines.

**Features:**
- Login
- Post List
- Post detail with comments
- My Posts
- Ability to see other users and their posts
- RTL support
- Offline support (Optional)
- Add animations (Optional)

**Guidelines:**
- Programming Language: Kotlin
- Use Material Theme
- Use databinding
- User Retrofit for Rest APIs
- Use MVVM pattern with architecture components
- Use koin or hilt for dependency injection
- Write clean and maintainable code
- Upload code in Git Repository

**API**: http://jsonplaceholder.typicode.com/guide/
